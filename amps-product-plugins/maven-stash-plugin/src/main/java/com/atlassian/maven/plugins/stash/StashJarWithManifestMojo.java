package com.atlassian.maven.plugins.stash;

import com.atlassian.maven.plugins.amps.JarWithManifestMojo;

import org.apache.maven.plugins.annotations.Mojo;

/**
 * @since 3.10
 */
@Mojo(name = "jar")
public class StashJarWithManifestMojo extends JarWithManifestMojo
{
}
